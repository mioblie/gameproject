import 'Monster.dart';

class Alatreon implements Monster {
  String? MonsterName = "Alatreon";
  String? TypeMonster = "Elder Dragons";
  String? Elements = "Dragon, Ice, Fire";
  int Hp = 5250;
  int Atk = 37;
  int OHK = 0;

  FRajang() {
    this.MonsterName = MonsterName;
    this.TypeMonster = TypeMonster;
    this.Elements = Elements;
    this.Hp;
    this.Atk;
  }

  //โดนตี
  Attacked(int Attacked) {
    Hp = Hp - Attacked;
    return Hp;
  }

  //บัพดาเมจ Monster
  int OneHitKill() {
    OHK = Atk * 3;
    return OHK;
  }
}
