import 'Monster.dart';

class FRajang implements Monster {
  String? MonsterName = "Furious Rajang";
  String? TypeMonster = "Large Monsters";
  String? Elements = "Thunder";
  int Hp = 4090;
  int Atk = 200;
  int DfTh = 0;

  FRajang() {
    this.MonsterName = MonsterName;
    this.TypeMonster = TypeMonster;
    this.Elements = Elements;
    this.Hp;
    this.Atk;
  }
//โดนตี
  Attacked(int Attacked) {
    Hp = Hp - Attacked;
    return Hp;
  }

//บัพดาเมจ Monster
  int DebuffThunder() {
    DfTh = Atk * 2;
    return DfTh;
  }
}
